#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 11:40:31 2019

@author: master
"""

from pymongo import MongoClient

class Restaurants(object): #Por problemas con el fichero de configuracion he trabajado con el root, sin usuario ni contraseña
    def __init__(self, url, port, auth): #user, password,
        self.url = url
        self.port = port
        #self.user = user
        #self.password = password
        self.auth = auth
        
    def connect(self):
        connection = MongoClient(self.url, self.port, authSource=self.auth) #usernemae=self.user, password=self.password
        return connection
    
    def search(self, **kwargs): #diccionario con todas las posibles opciones
        db = kwargs["connection"].master
        collection = db.restaurants
        
        if len(kwargs) == 4:
            if kwargs["control"]: 
                #control = True -> find(condition1).sort(condition2)
                item = collection.find(kwargs["conditionF"]).\
                sort(kwargs["conditionS"])
            else: 
                #control = False -> find(condition1, fields)
                item = collection.find(kwargs["conditionF"], kwargs["nameFields"])
        else:
            item = collection.find(kwargs["conditionF"])
            
        return item
        
        
def get_option():
    print("1. Display all the documents")
    print("2. Display all the restaurant which is in the borough Bronx")
    print("3. Find the restaurants who achieved a score more than 90")
    print("4. Find the restaurants which does not prepare any cuisine of \
          'American ' and achieved a grade point 'A' not belongs to the \
          borough Brooklyn. Sort by cuisine in descending order")
    print("5. Find the restaurant Id, name, borough and cuisine for those \
          restaurants which contains 'Reg' as three letters somewhere in its \
          name")
    print("6. Find the restaurant Id, name, borough and cuisine for those \
          restaurants which prepared dish except 'American' and 'Chinees' or\
          restaurant's name begins with letter 'Wil'")
    print("7. select all documents in the restaurants collection where the coord \
          field value is Double")
    print("0. Exit")
    
    option = int(input("Select one option "))
    return option
    
option = get_option()
explore = Restaurants('localhost', 27017, 'master')
conn = explore.connect()

while option != 0:
    if option == 1:
        result = explore.search(connection=conn, 
                                conditionF={})
        for doc in result:
            print(doc)
            
    if option == 2:
        result = explore.search(connection=conn, 
                                conditionF={"borough": "Bronx"})
        for doc in result:
            print(doc)
            
    if option == 3:
        result = explore.search(connection=conn, 
                                conditionF={"grades.score": {"$gt": 90}})
        for doc in result:
            print(doc)
            
    if option == 4:
        result = explore.search(connection=conn,
                                conditionF={"$and": [{"cuisine": {"$ne": "American"}},
                                                     {"grades.grade": "A"},
                                                     {"borough": {"$ne": "Brooklyn"}}]}, 
                                control=True, 
                                conditionS=[("cuisine",-1)])
        for doc in result:
            print(doc)
            
    if option == 5:
        result = explore.search(connection=conn, 
                                conditionF={"name": {"$regex": "Reg"}},
                                control=False,
                                nameFields={"restaurant_id":1, 
                                            "borough":1,
                                            "cuisine":1,
                                            "_id":0})
        for doc in result:
            print(doc)
            
    if option == 6:
        result = explore.search(connection=conn,
                                conditionF={"$or": [{"cuisine": {"$nin": ["American", "Chinese"]}},
                                                    {"name": "^Wil"}]},
                                control=False,
                                nameFields={"restaurant_id":1, 
                                            "borough":1,
                                            "cuisine":1,
                                            "_id":0})
        for doc in result:
            print(doc)
            
    if option == 7:
        result = explore.search(connection=conn,
                                conditionF={"address.coord": {"$type": "double"}})
        for doc in result:
            print(doc)
        
    option = get_option()