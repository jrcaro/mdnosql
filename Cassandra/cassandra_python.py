"""
Python  by Techfossguru
Copyright (C) 2017  Satish Prasad

"""
import logging
from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster, BatchStatement
from cassandra.query import SimpleStatement
import uuid
from datetime import date


class PythonCassandraExample:

    def __init__(self):
        self.cluster = None
        self.session = None
        self.keyspace = None
        self.log = None
        self.user_id = [uuid.uuid1(), uuid.uuid1(), uuid.uuid1(), uuid.uuid1(), uuid.uuid1()]

    def __del__(self):
        self.cluster.shutdown()

    def createsession(self):
        self.cluster = Cluster(['localhost'])
        self.session = self.cluster.connect(self.keyspace)

    def getsession(self):
        return self.session

    # How about Adding some log info to see what went wrong
    def setlogger(self):
        log = logging.getLogger()
        log.setLevel('INFO')
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s"))
        log.addHandler(handler)
        self.log = log

    # Create Keyspace based on Given Name
    def createkeyspace(self, keyspace):
        """
        :param keyspace:  The Name of Keyspace to be created
        :return:
        """
        # Before we create new lets check if exiting keyspace; we will drop that and create new
        rows = self.session.execute("SELECT keyspace_name FROM system_schema.keyspaces")
        if keyspace in [row[0] for row in rows]:
            self.log.info("dropping existing keyspace...")
            self.session.execute("DROP KEYSPACE " + keyspace)

        self.log.info("creating keyspace...")
        self.session.execute("""
                CREATE KEYSPACE %s
                WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
                """ % keyspace)

        self.log.info("setting keyspace...")
        self.session.set_keyspace(keyspace)

    def create_table(self):
        user = """
                    CREATE TABLE IF NOT EXISTS user (
                    user UUID,
                    name VARCHAR,
                    twitter_account VARCHAR,
                    description VARCHAR,
                    PRIMARY KEY (user)
                    );
                 """
        self.session.execute(user)
        self.log.info("user Table Created !!!")
        
        user_by_twitter = """
                    CREATE TABLE IF NOT EXISTS user_by_twitter (
                    user UUID,
                    name VARCHAR,
                    twitter_account VARCHAR,
                    description VARCHAR,
                    PRIMARY KEY (twitter_account)
                    );
                 """
        self.session.execute(user_by_twitter)
        self.log.info("user_by_twitter Table Created !!!")
        
        new_by_user_date = """
                    CREATE TABLE IF NOT EXISTS new_by_user_date (
                    user UUID,
                    title VARCHAR,
                    body VARCHAR,
                    date TIMESTAMP,
                    tags SET<VARCHAR>,
                    PRIMARY KEY (user, date)
                    );
                 """
        self.session.execute(new_by_user_date)
        self.log.info("new_by_user_date Table Created !!!")
        
        comment_by_user_date = """
                    CREATE TABLE IF NOT EXISTS comment_by_user_date(
                    user UUID,
                    comment TEXT,
                    date TIMESTAMP,
                    title TEXT,
                    PRIMARY KEY (user, date)
                    ) WITH CLUSTERING ORDER BY(date DESC);
                 """
        self.session.execute(comment_by_user_date)
        self.log.info("comment_by_user_date Table Created !!!")       

    # lets do some batch insert
    def insert_data(self):
        user_query = "INSERT INTO user (user, name, twitter_account, description) VALUES (%s, %s, %s, %s)"
        self.session.execute(user_query, (self.user_id[0],'Deanne Bengall','dbengall0','Female'))
        self.session.execute(user_query, (self.user_id[1],'Catherine Gotcliffe','cgotcliffe1','Female'))
        self.session.execute(user_query, (self.user_id[2],'Golda Malbon','gmalbon2','Female'))
        self.session.execute(user_query, (self.user_id[3],'Conney Stairmand','cstairmand3','Male'))
        self.session.execute(user_query, (self.user_id[4],'Saxon Crisford','scrisford6','Male'))
        
        twitter_query = "INSERT INTO user_by_twitter (user, name, twitter_account, description) VALUES (%s, %s, %s, %s)"
        self.session.execute(twitter_query, (self.user_id[0],'Deanne Bengall','dbengall0','Female'))
        self.session.execute(twitter_query, (self.user_id[1],'Catherine Gotcliffe','cgotcliffe1','Female'))
        self.session.execute(twitter_query, (self.user_id[2],'Golda Malbon','gmalbon2','Female'))
        self.session.execute(twitter_query, (self.user_id[3],'Conney Stairmand','cstairmand3','Male'))
        self.session.execute(twitter_query, (self.user_id[4],'Saxon Crisford','scrisford6','Male'))
        
        new_query = "INSERT INTO new_by_user_date (user, title, body, date, tags) VALUES (%s, %s, %s, %s, %s)"
        self.session.execute(new_query, (self.user_id[0],'Dozens dead in Delhi bag factory fire',\
                                         'A large fire has swept through a bag factory in the Indian capital Delhi, killing 43 workers, officials say.',\
                                         '2019-10-04 15:34:22', {'India','Dheli','Fire'}))
        self.session.execute(new_query, (self.user_id[1],'North Korea carries out \"very important test\"',\
                                         'North Korea says it has carried out a \"very important test\" at a satellite-launching site.',\
                                         '2019-03-17 22:52:14', {'Nuclear weapons','North Korea', 'Kim Jong-un'}))
        self.session.execute(new_query, (self.user_id[2],'Thousands join largest HK protest rally in months',\
                                         'Tens of thousands of protesters have marched through the streets of Hong Kong in the largest anti-government rally in months.',\
                                         '2019-08-14 02:15:23', {'Hong Kong', 'Anti-government protests', 'China'}))
        self.session.execute(new_query, (self.user_id[2],'Mike Horn and Boerge Ousland: North Pole explorers complete epic trek',\
                                         'Two explorers who trekked hundreds of miles at the North Pole and were running out of food have reached safety after an epic journey across the ice.',\
                                         '2019-11-09 23:59:02', {'Artic','Climate change','Norway'}))
        
        comment_query = "INSERT INTO comment_by_user_date (user, comment, date, title) VALUES (%s,%s,%s,%s)"
        self.session.execute(comment_query, (self.user_id[3],'Amazing! These guys are incredible','2019-11-10 18:30:20',\
                                             'Mike Horn and Boerge Ousland: North Pole explorers complete epic trek'))
        self.session.execute(comment_query, (self.user_id[3],'I feel really scared about this :(','2019-03-17 23:30:42',\
                                             'North Korea carries out \"very important test\"'))
        self.session.execute(comment_query, (self.user_id[4],'This is very sad','2019-10-06 16:08:22','Dozens dead in Delhi bag factory fire'))
        self.log.info('Insert Completed')

    def select_data(self):
        print('user')
        rows = self.session.execute('select * from user;')
        for row in rows:
            print(row.name, row.user)
        
        print()
        print('user_by_twitter')
        print()
        rows = self.session.execute('select * from user_by_twitter;')
        for row in rows:
            print(row.name, row.user)
        
        print()
        print('new_by_user_date')
        print()
        rows = self.session.execute('select * from new_by_user_date;')
        for row in rows:
            print(row.title, row.body, row.date, row.tags)
         
        print()
        print('comment_by_user_date')
        print()
        rows = self.session.execute('select * from comment_by_user_date;')
        for row in rows:
            print(row.comment, row.date, row.title)

    def update_data(self):
        pass

    def delete_data(self):
        pass


if __name__ == '__main__':
    masterpydb = PythonCassandraExample()
    masterpydb.createsession()
    masterpydb.setlogger()
    masterpydb.createkeyspace('masterpydb')
    masterpydb.create_table()
    masterpydb.insert_data()
    masterpydb.select_data()