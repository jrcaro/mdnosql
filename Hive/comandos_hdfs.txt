#Start ssh 
ssh root@127.0.0.1 -p 2222

#Create directory hdfs
hdfs dfs -mkdir /user/master

#Copy file from VM to HDFS. Local to VM -> scp -P 2222 popularNames.txt root@127.0.0.1:/home
hdfs dfs -ls /user/master

#Local to VM
scp -P 2222 weblogs_parse.txt root@127.0.0.1:/home

#Start hive
hive

#Create database
create database master;
use master;

#Create table
create external table restaurants (
address STRUCT<
building:STRING,
coord:array<DOUBLE>,
street:STRING,
zipcode:STRING>,
borough STRING,
cuisine STRING,
grades array<STRUCT<
date:STRUCT<date:TIMESTAMP>,
grade:STRING,
score:INT>>,
name STRING,
restaurant_id STRING)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION '/user/master';

#Load data (copy from VM to HDFS)
load data local inpath "/home/weblogs_parse.txt" into table weblogs;